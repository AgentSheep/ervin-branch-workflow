﻿using System;

namespace _ErvinGoh_BranchWorkFlow
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int numbers;
            int sum = 0;
            Console.WriteLine("Enter a Number");
            numbers = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= numbers; i++)
            {
                sum += i;
                Console.WriteLine(i);

            }
            Console.WriteLine("The sum is: " + sum);
            Console.ReadLine();
        }
    }
}
